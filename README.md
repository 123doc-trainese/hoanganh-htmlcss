# Bài tập html - css
## Yêu cầu
Hiểu được cấu trúc html, các thành phần, các thẻ html và chức năng của chúng
Cách sử dụng css trong html, mức độ ưu tiên của các style
Chuyển từ sử dụng css sang scss để generate ra css
Cách sử dụng boostrap và tailwindcss => sự khác nhau

## Thuc hanh
- xay dung giao dien trang qldt.hust.edu.vn

## Kien thuc thu duoc
#### Nắm được cấu trúc html và các thẻ
  Thứ tự ưu tiên css
  thứ tự ưu tiên cơ bản
  inline > id > class > tag
  Trong trường hợp cùng mức ưu tiên thì xét tới số lượng thẻ. nếu vẫn ngang bằng thì xét tới dòng css được viết sau sẽ được ưu tiên.

#### Nắm được cách sử dụng scss và phát scss sang css

#### Nắm được cách sử dụng Bootstrap và Tailwind css

Khác nhau giữa Bootstrap và Tailwind CSS?
-  Bootstrap:
++ Bootstrap cung cấp các công cụ ưu tiên thiết bị di động với bộ giao diện người dùng được tạo kiểu sẵn
++ người dùng có ít lựa chọn tùy chỉnh cho web của mình hơn
++ đầu ra của bootstrap tạo ra những web trông giống nhau vì nó theo một khung chung có sẵn

- Tailwind css:
++ Tailwind cung cấp các widget được thiết kế sẵn để tạo điều kiện cho các nhà phát triển tạo một trang web từ đầu
++ Người dùng được tùy chọn nhiều hơn
++ đầu ra của taiwind tạo ra một giao diện người dùng nguyên bản, gọn gàng và theo ý mình

  
Thực hiện bởi [Nguyen Hoang Anh](https://github.com/anhbk177)

## Liên kết
 
[hoanganh-html-css](https://gitlab.com/123doc-trainese/hoanganh-htmlcss)
